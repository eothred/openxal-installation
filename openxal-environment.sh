#!/bin/bash
# Set up needed environmental variables
#
# Authors: miroslav.pavleski@cosylab.com,
#   ivo.list@cosylab.com
#

# 0. Find the base dir of the script and set OPENXAL_HOME
BASE_DIR="${BASH_SOURCE[0]}";
cd "`dirname "${BASE_DIR}"`" > /dev/null
BASE_DIR="`pwd`";
cd - > /dev/null

export OPENXAL_HOME=$BASE_DIR

# 1. ESS OpenXAL flavour optionaly can use this environment variable to load default system-wide configuration
export OPENXAL_CONFIG_DIR=$BASE_DIR/etc

# 2. Openxal version
export OPENXAL_VERSION=1.0.9

# 3. OpenXAL library path
export OPENXAL_LIBRARY=$BASE_DIR/lib/openxal/openxal.library-${OPENXAL_VERSION}.jar

# 4. Jython classpath setup
# Jython version on CODAC 4.1 is 2.2.1. so JYTHONPATH actually won't be used 
export JYTHONPATH=$JYTHONPATH:${OPENXAL_LIBRARY}

# Hack for Jython prior to 2.5 to use the JYTHONPATH
alias jython='java -jar $BASE_DIR/jython/jython-standalone-2.5.3.jar -Dpython.path=$JYTHONPATH'

# 5. JRuby class path setup
export JRUBY_CP=$JRUBY_CP:${OPENXAL_LIBRARY}

# 6. Java classpath setup
export CLASSPATH=${OPENXAL_LIBRARY}:$CLASSPATH

# 7. Workaround for MAC java home
if [[ -x /usr/libexec/java_home ]]; then
  export JAVA_HOME=$(/usr/libexec/java_home)
fi

