#!/bin/bash
# Create and set up database for use by openxal-gui
# Should be run  as sudo {PATH}/openxal-gui-db-installation.sh
# Author: blaz.kranjc@cosylab.com,
#

. $(dirname "${BASH_SOURCE[0]}")/../openxal-environment.sh

if id -u "postgres" >/dev/null 2>&1; then   # workaround for installations without postgres user
        SUDO="sudo -u postgres"
else
        SUDO="sudo"
fi

if ! command -v psql >/dev/null 2>&1; then
  if [ -f /Library/PostgreSQL/9.4/bin/psql ]; then
    PSQL=/Library/PostgreSQL/9.4/bin/psql
  fi
  if [ -f /Applications/Postgres.app/Contents/Versions/9.4/bin/psql ]; then
    PSQL=/Applications/Postgres.app/Contents/Versions/9.4/bin/psql
  fi
fi

$SUDO -v || exit 1   # ask for password
SUDO="$SUDO -n"

cd /   # prevents psql displaying error to change to current dir

if $SUDO psql -lqt | cut -d \| -f 1 | grep -qw $DATABASE
then
    echo "Database $DATABASE already exists, skipping initialization"
    exit 0
fi

echo "--Creating user $USER."
$SUDO psql $OPTIONS -c "CREATE USER $USER" || exit 1

echo "--Setting user password to $PASSWORD."
$SUDO psql $OPTIONS  -c "ALTER USER $USER PASSWORD '$PASSWORD'"

echo "--Creating database $DATABASE"
$SUDO psql $OPTIONS  -c "CREATE DATABASE $DATABASE"

echo "--Creating schema $SCHEMA"
$SUDO psql $OPTIONS -d $DATABASE -c "CREATE SCHEMA \"$SCHEMA\" AUTHORIZATION $USER"

echo "--Runing configuration file $CONFIG_FILE."
PGPASSWORD=$PASSWORD
export PGPASSWORD
$PSQL -U $USER -h localhost --quiet -d $DATABASE  -f $CONFIG_FILE


echo "--Database configuration finished."

