package se.lu.esss.ics.openxal.tutorial.acceleratorapp.step1;


import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import xal.extension.application.smf.AcceleratorWindow;
import xal.extension.widgets.smf.XALSynopticPanel;
import xal.extension.widgets.plot.FunctionGraphsJPanel;

/**
 * It defines a window for the document.
 * The document window class must define the main window used to present the document to the user.
 * The developer is free to lay out the window as desired
 */
public class Window extends AcceleratorWindow {
	private static final long serialVersionUID = 1L;
	
	private XALSynopticPanel synopticPanel; /** panel to display structure of the accelerator */
	private JTable table; /** Table for the modifiable parameters */
	private FunctionGraphsJPanel graph; /** Graph of the simulation */
	
	/**
	 * Creates a window presenting the document
	 * @param document The document to load
	 */
	public Window(Document document) {
		super(document);
		this.document = document;
		setSize(600, 600);
		makeContent(document);
	}	

	/**
	 * Lays out the window.
	 */
	protected void makeContent(final Document document) {        	    
		// first we set up synoptic panel
		synopticPanel = new XALSynopticPanel();					    
	    synopticPanel.setPreferredSize(new Dimension(100,100));
	    
	    // we set up the table
	    table = new JTable();
	    
	    // we set up the graph
	    graph = new FunctionGraphsJPanel();
     	graph.setAxisNames("position", "beta");
     	graph.setLegendVisible(true);        	
	    
     	// following is the master layout 
	    JTabbedPane tabbedPane = new JTabbedPane();
	    tabbedPane.add("Table", new JScrollPane(table));	         	
	    tabbedPane.add("Graph", graph);
	    
	    JPanel masterPanel = new JPanel(new BorderLayout());	    
	    masterPanel.add(synopticPanel, BorderLayout.NORTH);	    
	    masterPanel.add(tabbedPane);
	    
	    getContentPane().add(masterPanel);
	}  
}
