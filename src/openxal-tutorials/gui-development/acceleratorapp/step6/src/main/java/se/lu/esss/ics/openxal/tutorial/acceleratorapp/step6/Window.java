package se.lu.esss.ics.openxal.tutorial.acceleratorapp.step6;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import xal.smf.AcceleratorSeq;
import xal.extension.application.smf.AcceleratorWindow;
import xal.extension.widgets.smf.XALSynopticPanel;
import xal.extension.widgets.plot.FunctionGraphsJPanel;

/**
 * It defines a window for the document.
 * The document window class must define the main window used to present the document to the user.
 * The developer is free to lay out the window as desired
 */
public class Window extends AcceleratorWindow {
	private static final long serialVersionUID = 1L;
	
	private XALSynopticPanel synopticPanel; /** panel to display structure of the accelerator */
	private JTable table; /** Table for the modifiable parameters */
	private FunctionGraphsJPanel graph; /** Graph of the simulation */
	
	/**
	 * Creates a window presenting the document
	 * @param document The document to load
	 */
	public Window(Document document) {
		super(document);
		this.document = document;
		setSize(600, 600);
		makeContent(document);
	}	

	/**
	 * Lays out the window.
	 */
	protected void makeContent(final Document document) {        	    
		// first we set up synoptic panel
		synopticPanel = new XALSynopticPanel();					    
	    synopticPanel.setPreferredSize(new Dimension(100,100));
	    synopticPanel.setAcceleratorSequence(document.getSelectedSequence());
	    synopticPanel.addMouseListener(new MouseAdapter() {
	    	/**
	    	 * When the user selects an element in the accelerator the same row in the table is selected.
	    	 * */
			@Override
			public void mouseClicked(MouseEvent event) {
				// here a trick is used to figure out the element via tool tip text
				String id = synopticPanel.getToolTipText(event);
				Document.ParametersTableModel parametersTableModel = document.getParametersTableModel();
				// following code searches for the appropriate element
				// this could be done better using a map, but it's not performance critical
				int s,e,n=parametersTableModel.getRowCount();
				for (s = 0; s<n; s++)
					if (id.equals(parametersTableModel.getValueAt(s, 0))) break;
				for (e = s; e<n; e++)
					if (!id.equals(parametersTableModel.getValueAt(e, 0))) break;
				if (s<n) {
					table.setRowSelectionInterval(s,e-1);
					table.scrollRectToVisible(table.getCellRect(s, 0, true));
				}
			}			    	
	    });

	    // we set up the table
	    table = new JTable(document.getParametersTableModel());
	    
	    // we set up the graph
	    graph = new FunctionGraphsJPanel();
     	graph.setAxisNames("position", "beta");
     	graph.setLegendVisible(true);
    	graph.addGraphData(document.getTwissX());
        graph.addGraphData(document.getTwissY());
        graph.addGraphData(document.getTwissZ());
	    
     	// following is the master layout 
	    JTabbedPane tabbedPane = new JTabbedPane();
	    tabbedPane.add("Table", new JScrollPane(table));	         	
	    tabbedPane.add("Graph", graph);
	    
	    JPanel masterPanel = new JPanel(new BorderLayout());	    
	    masterPanel.add(synopticPanel, BorderLayout.NORTH);	    
	    masterPanel.add(tabbedPane);
	    
	    getContentPane().add(masterPanel);
	}
	
	/**
	 * This procedure is called when the sequence is changed.
	 * */
	public void selectedSequenceChanged(AcceleratorSeq sequence) {
		synopticPanel.setAcceleratorSequence(sequence);
	}
}
