package se.lu.esss.ics.openxal.template.acceleratorapp;

import java.net.URL;

import xal.extension.application.Commander;
import xal.extension.application.XalWindow;
import xal.extension.application.smf.AcceleratorDocument;

/**
 * Document class is a holder for document contents.
 * It provides the framework with document specific information 
 * and also provides hooks for document control. 
 * The application framework allows the developer the freedom to encode their document as they wish.
 */
public class Document extends AcceleratorDocument {

	/**
	 *  Empty constructor to create empty document
	 */
	public Document()
	{
	}
	
	/**
	 * This method is called when the window needs to be shown.
	 * Although dirty, variable mainWindow has to be set to point to the window representing this document.
	 * @see xal.application.XalAbstractDocument#makeMainWindow()
	 */
	@Override
	protected void makeMainWindow() {	
		mainWindow = new Window(this);
	}

	/**
	 * This method saves the content of this document.
	 * @see xal.application.XalAbstractDocument#saveDocumentAs(java.net.URL)
	 * 
	 * @param url Path to the file to which to save the document.
	 */
	@Override
	public void saveDocumentAs(URL url) {
		// TODO Implement code to save the document

	}	
	
	/**
	 * This method is provided as a helper in the template to load the content.
	 * 
	 *  @param url path to the file to load
	 * */
	public static Document load(URL url)
	{
		Document document = new Document();
		// TODO implement code to load the document
		return document;
	}
	
	/**
	 * Adds actions to the menus
	 */
	@Override
	protected void customizeCommands(Commander commander) {		
		// TODO add menu actions	
	}
}