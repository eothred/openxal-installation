package se.lu.esss.ics.openxal.template.desktopapp;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import xal.extension.application.Application;
import xal.extension.application.Commander;
import xal.extension.application.DesktopApplication;
import xal.extension.application.DesktopApplicationAdaptor;
import xal.extension.application.XalInternalDocument;

/**
 * The Main class is an entry point for the application.
 * This is a desktop application, i.e. containing multiple document windows inside main window. 
 * */
public class Main extends DesktopApplicationAdaptor {
	protected String[] documentTypes = new String[]{}; // TODO add document types the application supports
		
	/**
	 * Returns a new instance of application's document class 
	 * @see xal.application.DesktopApplicationAdaptor#newEmptyDocument()
	 * 
	 * @return application's document class
	 */
	@Override
	public XalInternalDocument newEmptyDocument() {
		return new Document();
	}

	/** Returns an instance of application's document class with specific file 
	 * @see xal.application.DesktopApplicationAdaptor#newDocument(java.net.URL)
	 * 
	 * @param url Path to the file to be opened
	 * @return application's document class with the file opened
	 */
	@Override
	public XalInternalDocument newDocument(URL url) {
		return Document.load(url);
	}

	/** 
	 * Returns an array of file extensions of readable files.
	 * @see xal.application.AbstractApplicationAdaptor#readableDocumentTypes()
	 * 
	 * @return array of file extensions of writable files
	 */
	@Override
	public String[] readableDocumentTypes() {
		return writableDocumentTypes();
	}

	/**
	 * Returns an array of file extensions of writable files
	 * @see xal.application.AbstractApplicationAdaptor#writableDocumentTypes()
	 * 
	 * @return array of file extensions of writable files
	 */
	@Override
	public String[] writableDocumentTypes() {
		return documentTypes;
	}

	/**
	 * Returns application name
	 * @see xal.application.AbstractApplicationAdaptor#applicationName()
	 * 
	 * @return the application name
	 */
	@Override
	public String applicationName() {		
		return ""; // TODO return application name
	}
	
	/**
	 * Main entry point of the application
	 * 
	 * @param args Application arguments
	 */
	public static void main(String[] args) {
		try {
		    //TODO add application specific initialization code			
		    setOptions( args );
		    System.out.println("Starting application...");
		    Logger.getLogger("global").log( Level.INFO, "Starting application..." );
		    DesktopApplication.launch(new Main());
		}
		catch(Exception exception) {
		    Logger.getLogger("global").log( Level.SEVERE, "Error starting application.", exception );
		    System.err.println( exception.getMessage() );
		    exception.printStackTrace();
		    Application.displayApplicationError("Launch Exception", "Launch Exception", exception);
		    System.exit(-1);
		}
	}
	
	/**
	 * Adds actions to the menus
	 */
	@Override
	protected void customizeCommands(Commander commander) {		
		// TODO add menu actions		
	}
}
