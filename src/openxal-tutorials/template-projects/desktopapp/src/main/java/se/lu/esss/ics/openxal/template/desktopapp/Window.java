package se.lu.esss.ics.openxal.template.desktopapp;

import xal.extension.application.XalInternalWindow;

/**
 * It defines a window for the document. The document window class must define
 * the main window used to present the document to the user. The developer is
 * free to lay out the window as desired
 */
public class Window extends XalInternalWindow {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a window presenting the document
	 * 
	 * @param document
	 *            The document to load
	 */
	public Window(Document document) {
		super(document);
		setSize(600, 600);
		makeContent(document);
	}

	/**
	 * Lays out the window.
	 */
	protected void makeContent(Document document) {
		// TODO implement code to lay out the window.
		// getContentPane().add(...);
	}
}
