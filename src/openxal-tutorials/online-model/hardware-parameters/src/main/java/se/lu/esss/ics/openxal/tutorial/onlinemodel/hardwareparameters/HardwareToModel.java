package se.lu.esss.ics.openxal.tutorial.onlinemodel.hardwareparameters;

import xal.smf.Accelerator;
import xal.smf.AcceleratorSeq;
import xal.smf.AcceleratorNode;
import xal.smf.data.XMLDataManager;
import java.util.List;
import java.util.Iterator;
public class HardwareToModel {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Load the configuration files
		XMLDataManager.setDefaultPath("src/main/resources/main.xal"); 
		// Load the SMF 
		Accelerator accelerator = XMLDataManager.loadDefaultAccelerator();
		// Define the sequence one is interesting in, in this example MEBT sequence from the SNS accelerator
		String sequenceName = "MEBT";
		// Select only the sequence one is interesting in, from all the components in the accelerator
		AcceleratorSeq sequence = accelerator.getSequence(sequenceName);
		// As an example of things one can do now, print the node name for all the nodes in the MEBT sequence 
		List<AcceleratorNode> nodeList = sequence.getAllNodes();
		Iterator<AcceleratorNode> iterator = nodeList.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next().toString());
		}
	}

}
